# prosECCo75

prosECCo75 force field using the physically sound 0.75 charge scaling.

## Name
proECCo75 is a derivative of [CHARMM36 force field](http://mackerell.umaryland.edu/charmm_ff.shtml#gromacs) that aims to solve the problem of excessive binding between charged groups in non-polarizable classical molecular dynamics (MD) simulations.

## Authors and acknowledgment
Please cite prosECCo75 paper when using the force field. Please do not forget to indicate the version you are using to better track any issues.

**prosECCo75 biomolecular force field**

Nencini et al. Effective Inclusion of Electronic Polarization Improves the Description of Electrostatic Interactions: The prosECCo75 Biomolecular Force Field. J. Chem. Theory Comput. 2024, 20, 17, 7546–7559. https://doi.org/10.1021/acs.jctc.4c00743

**Additions/updates to prosECCo75**

Riopedre-Fernandez et al. Developing and Benchmarking Sulfate and Sulfamate Force Field Parameters via Ab Initio Molecular Dynamics Simulations To Accurately Model Glycosaminoglycan Electrostatic Interactions. J. Chem. Inf. Model. 2024, ASAP. https://doi.org/10.1021/acs.jcim.4c00981

**WARNING:** prosECCo75 is a derivative work of the CHARMM36 force field. When using proECCo75 also cite all relevant CHARMM36 papers of the molecular moieties used. Without CHARMM36 and the excellent work it provides, prosECCo75 will not exist.

## License
The CHARMM36 files are licensed as the original authors intended. The prosECCo75 modifications are licensed with [Creative Commons Attribution 4.0] (https://creativecommons.org/licenses/by/4.0/legalcode)

## Project status
prosECCo75 is under active development and accepting contributors. We accept pull requests.
